import json
import re
import chardet
from collections import Counter


def cleantext(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, ' ', raw_html)
    cleanr = re.compile('/.*?/')
    cleantext = re.sub(cleanr, ' ', cleantext)
    cleantext = re.sub(r'\W', ' ', cleantext)
    cleantext = re.sub(r'\d', '', cleantext)
    cleantext = re.sub(r'\s+', ' ', cleantext)
    return cleantext


def import_from_json(file_name='newsafr.json'):
    with open(file_name, 'rb', encoding=None) as raw_file:
        raw_data = raw_file.read()
        result = chardet.detect(raw_data)
        data_file =raw_data.decode(result['encoding'])
        data = json.loads(data_file)
        return data


def combine_news_to_one_text(data):
    text_all = ''
    for news in data['rss']['channel']['item']:
        try:
            clean_text = cleantext(news['description']['__cdata'])
        except TypeError:
            clean_text = cleantext(news['description'])
        text_all += clean_text.lower()
    return text_all


def top_result(words, file_name):
    top_words = words.most_common(10)
    print("\nТоп 10 слов из {} встречающиеся в тексте:".format(file_name))
    for word in top_words:
        name, count = word
        print(name, count)


def parse_all_files():
    files_to_read = ['newsafr.json', 'newscy.json', 'newsfr.json', 'newsit.json']
    for file_name in files_to_read:
        data = import_from_json(file_name)
        text = combine_news_to_one_text(data)
        words = Counter(word for word in re.findall(r'[\w+]{6,}', text))
        top_result(words, file_name)


parse_all_files()
